# FSL2023

Repositório para o site da edição de 2023 da [Festa do Software Livre](https://softwarelivre.eu/).

Este evento vai decorrer em Aveiro, de 15 a 17 de Setembro.

## Licença

Como podem ver no ficheiro [LICENSE](./LICENSE), este sítio Web está licenciado com a licença GNU Affero General Public License Version 3 (AGPLv3).
